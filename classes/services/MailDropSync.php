<?php

require 'vendor/autoload.php';
use Maildrop\Maildrop;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Db;

class MailDropSync {
    
    private $log;

    public function __construct($apiKey) {
        Maildrop\Maildrop::setDefaultClientApiKey("-------------------------");
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler('mailDrop.log', Logger::WARNING));
    }

    /**
     * This method syncs Prestashop database customers with MailDrop.
     * It starts by fetching customer data from the Prestashop database,
     * then it iterates over this data to add or update customers in MailDrop.
     *
     * @throws Exception If an error occurs during synchronization.
     */
    public function syncCustomersWithMailDrop() {
        $customerData = $this->getCustomerDataFromDatabase();
    
        $md = new Maildrop();
    
        foreach ($customerData as $groupName => $groupCustomers) {
            $groupContacts = [];
        
            foreach ($groupCustomers as $individualCustomer) {
                $groupContacts[] = [
                    "Email" => $individualCustomer["Email"],
                    "First_Name" => $individualCustomer["First_Name"],
                    "Last_Name" => $individualCustomer["Last_Name"],
                    "Customs_Fields" => $individualCustomer["Customs_Fields"]
                ];
            }
        
            // Import batch
            $response = $md->subscribers()->import_batch([
                "listid" => $individualCustomer["ListID"],
                "batch" => $groupContacts,
                "options" => [
                    "update_existing" => true,
                    "resubscribe" => true
                ]
            ]);
        
            // Check response
            if (!$response->success()) {
                $this->log->error("Error adding customers from category '$groupName' : " . $response->getErrorMsg());
            } else {
                $this->log->info("Successfully synchronized customers from category '$groupName'.");
            }
        }
        
    }
        


    /**
     * This method retrieves customer data from the database.
     *
     * @return array The customer data.
     */
    private function getCustomerDataFromDatabase() {

        $sql_query = "
        SELECT c.email, c.firstname, c.lastname, c.date_add, c.id_default_group as id_group, c.company, a.postcode
        FROM " . _DB_PREFIX_ . "customer c 
        INNER JOIN " . _DB_PREFIX_ . "address a ON c.id_customer = a.id_customer 
        INNER JOIN " . _DB_PREFIX_ . "customer_group cg ON c.id_customer = cg.id_customer
        GROUP BY c.id_customer";
    
        $results = Db::getInstance()->executeS($sql_query);
    
        
        
        // Initialize an associative array to store the customer data sorted by category
        $customer_data = [
            "Collectivités" => [],
            "Durolage" => [],
            "Particuliers" => [],
            "Professionnels" => [],
        ];

            // Mapping des groupes
            $group_mapping = [
                "Collectivités" => [
                    "id" => [14, 16, 17, 18, 19, 26, 29, 37], 
                    "list_id" => "0ba0761fc67612d28ee43336183009cb", 
                    "customs_fields" => [
                        "postcode" => "postcode", 
                        "sous-groupe" => "Sous-groupe"
                    ],
                ],
                "Durolage" => [
                    "id" => [43, 49], 
                    "list_id" => "c1af8c4c7da623977debd8b4cf84b109", 
                    "customs_fields" => [
                        "entreprise" => "company",
                    ],
                ],
                "Professionnels" => [
                    "id" => [6, 7, 8, 9, 10, 11, 12, 13, 15, 20, 21, 22, 23, 24, 25, 27, 28, 31, 35, 38, 40, 46, 47, 50, 51], 
                    "list_id" => "0651b15a19115028b223e1de84ed8289", 
                    "customs_fields" => [
                        "entreprise" => "company",
                        "postcode" => "postcode", 
                        "inscription" => "date_add"
                    ],
                ],
                "Particuliers" => [
                    "id" => [], // Particuliers are by default, so there's no need to specify the group ids here.
                    "list_id" => "89a6a63f56e403f8d933a05222107b8b", 
                    "customs_fields" => [
                        "postcode" => "postcode"
                    ],
                ],
            ];
        
        // Iterate over the results to build an associative array with the fields
        foreach ($results as $row) {
            // Definition of data common to all customer categories
            $common_data = [
                "Email" => $row["email"],
                "First_Name" => $row["firstname"],
                "Last_Name" => $row["lastname"],
            ];

            // Variable to track if the customer's group has been found
            $found = false;

            // Determine the customer's group
            foreach($group_mapping as $group => $details) {
                if($group != 'Professionnels' && in_array($row['id_group'], $details['id'])) {
                    $found = true;
                    $common_data["ListID"] = $details['list_id'];

                    // Build the custom fields
                    $custom_fields = [];
                    foreach($details['customs_fields'] as $maildropField => $databaseField) {
                        $custom_fields[$maildropField] = $row[$databaseField];
                    }
                    $common_data["Customs_Fields"] = $custom_fields;

                    // Add the customer data to the array.
                    $customer_data[$group][] = $common_data;
                }
            }

            // If the customer's group has not been found, he is considered a professional
            if(!$found && isset($group_mapping['Professionnels'])) {
                $group = 'Professionnels';
                $details = $group_mapping[$group];
                $common_data["ListID"] = $details['list_id'];

                // Build the custom fields
                $custom_fields = [];
                foreach($details['customs_fields'] as $maildropField => $databaseField) {
                    $custom_fields[$maildropField] = $row[$databaseField];
                }
                $common_data["Customs_Fields"] = $custom_fields;

                // Add the customer data to the array.
                $customer_data[$group][] = $common_data;
            }
        }


        return $customer_data;
    }
}

