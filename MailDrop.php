<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

require 'vendor/autoload.php';

use Db;
use Maildrop\Maildrop;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
require 'classes/services/MailDropSync.php';

class Maildrop extends Module
{
    public function __construct()
    {
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler('mailDrop.log', Logger::WARNING));

        $this->name = 'Maildrop';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'NG';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Maildrop');
        $this->description = $this->l('Description.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('Maildrop')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('displayBackOfficeHeader')) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        return true;
    }

    public function hookActionObjectCustomerUpdateAfter($params)
    {
        // Check if the newsletter subscription status has changed
        if ($params['object']->newsletter != $params['object']->getFields()['newsletter']) {
            $customerId = (int)$params['object']->id;

            $sql = 'SELECT id_group FROM ' . _DB_PREFIX_ . 'customer_group WHERE id_customer = ' . $customerId;
            $result = Db::getInstance()->executeS($sql);
            $groupId = $result[0]['id_group'];            

            // Create a mapping between PrestaShop group IDs and MailDrop list IDs
            $listIds = [
                'Collectivités'   => '0ba0761fc67612d28ee43336183009cb',
                'Durolage'        => 'c1af8c4c7da623977debd8b4cf84b109',
                'Professionnels'  => '0651b15a19115028b223e1de84ed8289',
                'Particuliers'    => '89a6a63f56e403f8d933a05222107b8b',
            ];

            $groups = [
                'Collectivités'   => ['14', '16', '17', '18', '19', '26', '29', '37'],
                'Durolage'        => ['43', '49'],
                'Professionnels'  => ['6', '7', '8', '9', '10', '11', '12', '13', '15', '20', '21', '22', '23', '24', '25', '27', '28', '31', '35', '38', '40', '46', '47', '50', '51'],
            ];

            $groupToListMap = [];

            foreach ($groups as $listName => $groupIds) {
                foreach ($groupIds as $groupId) {
                    $groupToListMap[$groupId] = $listIds[$listName]; }
                }

            // Default group for all others: Particuliers
            $groupToListMap['default'] = $listIds['Particuliers'];
            
            $md = new Maildrop();

            // Check if the customer's group has a list mapping
            if (isset($groupToListMap[$groupId])) {
                $listId = $groupToListMap[$groupId];

                if ($params['object']->newsletter == 1) {
                    // If the customer is subscribed to the newsletter, add them to the MailDrop list
                    $response = $md->subscribers()->add([
                        "listid" => $listId,
                        "email" => $params['object']->email
                    ]);
    
                    if (!$response->success()) {
                        $errorMsg = "Error while subscribing user: " . $response->getErrorMsg();
                        $this->log->error($errorMsg);
                    }
                } else {
                    // If the customer is unsubscribed from the newsletter, remove them from the MailDrop list
                    $response = $md->subscribers()->unsubscribe([
                        "listid" => $listId,
                        "emails" => [
                            $params['object']->email
                        ],
                    ]);

                    if (!$response->success()) {
                        $errorMsg = "Error while unsubscribing user: " . $response->getErrorMsg();
                        $this->log->error($errorMsg);
                    }
                }
            }
        }
    }
}
